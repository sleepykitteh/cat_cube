# cat cube
opengl rotating cube that you can move with w, a, s, and most importantly, d, and has a cat texture and music (and it is very good.)

![preview of the upstream ferret cube](https://i.imgur.com/kXxNEcb.mp4)

# differences between this and ferret cube
i added a cat
this uses gcc instead of clang

# Compilation
but first, important notice:
i have no idea what the hell i'm doing
i have no idea how to program in c (or whatever language this is programmed in)
with that out of the way, carry on :3

# ur dependencies:
`base-devel`, your GPU's opengl implementation (`mesa` for amd and intel, nvidia-utils is pretty self explanatory), `glfw-dev`, `cglm`, `openal-soft`, `freealut`

arch linux example: `~ yay -S mesa glfw-x11 cglm openal freealut`
debian example: `$ apt install (placeholder because i dont run a debian machine rn lmao)`
(this example uses yay, as cglm is not in the official repos for arch at the moment)

# compiling commandz
`make`
`make run`