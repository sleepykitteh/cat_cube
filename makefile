TARGET=cat_cube
CC=gcc
CFLAGS=-g -Wall
LD=-lGL -lglfw -lm -lalut -lopenal -lpthread

all: src/main.c
	mkdir -p bin
	$(CC) src/main.c $(CFLAGS) $(LD) -o bin/$(TARGET)
run:
	@bin/$(TARGET)
clean:
	rm -f bin/*
