#define GL_GLEXT_PROTOTYPES
#define STB_IMAGE_IMPLEMENTATION

#include "../include/stb_image.h"
#include <GLFW/glfw3.h>
#include <cglm/cglm.h>
#include <AL/alut.h>
#include <GL/gl.h>
#include <stdio.h>

const char texturePath[] = "asset/cta jitter.png";
const char musicPath[] = "asset/cat in a cube remix.wav";

const unsigned int window_width = 640;
const unsigned int window_height = 640;

float X = 0.0f;
float Y = 0.0f;

const float triangleVertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
};

// Vertex Shader makes shapes such as triangles.
const char *vertexShaderSource = "\
    #version 330 core\n\
    layout (location = 0) in vec3 aPos;\n\
    layout (location = 1) in vec2 aTexCoord;\n\
    out vec2 texCoord;\n\
    uniform mat4 transform;\n\
    void main()\n\
    {\n\
        gl_Position = transform * vec4(aPos.xyz, 1.0f);\n\
        texCoord = aTexCoord;\n\
    }\n\
    ";

// Fragment Shader gives color to shapes.
const char *fragmentShaderSource = "\
    #version 330 core\n\
    out vec4 FragColor;\n\
    in vec2 texCoord;\n\
    uniform vec3 testUniform;\n\
    uniform sampler2D ourTexture;\n\
    void main() \n\
    {\n\
        FragColor = texture(ourTexture, texCoord);\n\
    }\n\
    ";

// When user press a key this will be called.
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    switch (key) {
        case GLFW_KEY_W:
            Y += 0.05f;
        break;

        case GLFW_KEY_S:
            Y -= 0.05f;
        break;

        case GLFW_KEY_A:
            X -= 0.05f;
        break;

        case GLFW_KEY_D:
            X += 0.05f;
        break;
    };
}

// When window is resized this will be called, change viewport size to new size.
void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

int main() {
    // Initialize GLFW:
    // ----------------
    glfwInit();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(window_width, window_height, "cat cube", NULL, NULL);
    if (window == NULL) {
        printf("GLFW failed to create window!\n");
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glEnable(GL_DEPTH_TEST);

    glfwSetKeyCallback(window, key_callback);

    // Build and compile Shaders:
    // --------------------------
    const unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);

    const unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);

    // Did the shaders compile correctly?
    int success;
    char infoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        printf("Vertex shader did not compile correctly! %s\n", infoLog);
    }
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        printf("Fragment shader did not compile correctly! %s\n", infoLog);
    }

    // Link the shaders, eg: tell GLFW what shaders to use.
    const unsigned int shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);

    // Did the linking work correctly?
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        printf("Shader program did not link correctly! %s\n", infoLog);
    }

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    // VBO Is used to send data to GPU.
    // VAO is needed for vertex attributes, eg: vertex shader variable.
    unsigned int VBO, VAO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // Texture attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // glBufferData tells GLFW what the shaders are supposed to show to the screen.
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STREAM_DRAW);

    // Textures:
    // ---------
    // Load the texture using stb_image.h library.
    int width, height, nrChannels;
    unsigned char *data = stbi_load(texturePath, &width, &height, &nrChannels, 0);

    // Generate texture object for OpenGL.
    unsigned int texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    // Tell GLFW we are working with 2D textures and that it should mirror repeat the texture in both X,Y (S,T) coordinates.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

    // Tell GLFW we are working with 2D textures and that they have mipmaps which use GL_NEAREST and not GL_LINEAR for texture filtering.
    // GL_NEAREST produces rougher results while GL_LINEAR produces smoother ones.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    if (data) {
        // Input the texture data we got from stbi_load into the texture object and then generate the mipmap.
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else {
        printf("Failed to load texture!\n");
    }

    // Free the memory used for the image.
    stbi_image_free(data);
    alutInit(NULL, NULL);

    /* Generate a single source, joindre the buffer to it and start playing. */
    ALuint buffer = alutCreateBufferFromFile(musicPath);
    ALuint musicSource;
    ALint musicStatus;

    alGenSources (1, &musicSource);
    alSourcei(musicSource, AL_BUFFER, buffer);

    // Rendering Loop, where the magic happens:
    // ----------------------------------------
    while (!glfwWindowShouldClose(window)) {
        alGetSourcei(musicSource, AL_SOURCE_STATE, &musicStatus);
        if (musicStatus != AL_PLAYING) {
            alSourcePlay(musicSource);
        }

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(shaderProgram);

        glBindTexture(GL_TEXTURE_2D, texture);
        glBindVertexArray(VAO);

        mat4 transform = {
            {1, 0, 0, 0},
            {0, 1, 0, 0},
            {0, 0, 1, 0},
            {0, 0, 0, 1}
        };

        glm_translate(transform, (vec3){X, Y, 0.0f});
        //glm_rotate(transform, (float)(pow(glfwGetTime(), 1.5)), (vec3){0.0f, 1.0f, 1.0f});
        glm_rotate(transform, glm_rad((float)(glfwGetTime()) * 360), (vec3){fabs(cos((float)(glfwGetTime())/3.56)), fabs(cos((float)(glfwGetTime())/1.35)), cos((float)(glfwGetTime())*1.54)});

        unsigned int transformLoc = glGetUniformLocation(shaderProgram, "transform");
        glUniformMatrix4fv(transformLoc, 1, GL_FALSE, (float *)transform);

        glDrawArrays(GL_TRIANGLES, 0, 36);
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // Cleanup, now that the program will terminate we can clean shaders and such:
    // ---------------------------------------------------------------------------
    glDeleteBuffers(1, &VAO);
    glDeleteVertexArrays(1, &VAO);
    glDeleteProgram(shaderProgram);
    glfwTerminate();

    if (!alutExit ()) {
        ALenum error = alutGetError();
        fprintf(stderr, "%s\n", alutGetErrorString (error));
        exit(EXIT_FAILURE);
    }
    return 0;
}
